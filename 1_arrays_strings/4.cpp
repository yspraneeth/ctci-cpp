/* Palindrome Permutation
* Given a string, write a function to check if it is a permutation
* of a palindrome. A palindrome is a word or phrase that is the same
* forwards or backwards. A permutation is a rearrangement of letters.
* The palindrome doesn't need to be limited to just dictionary words.
*
* Command: g++ -std=c++11 -o 4 4.cpp
*/

#include <iostream>
#include <string>
#include <unordered_map>

// If the length of the string is even, if all the character counts are even,
// then, it will be a palindrome of a permutation.
// If the length of the string is odd, then there can only be 1 char count that is odd
// and the rest are all even for the permutation to be a palindrome
// Time complexity: O(n)
// Space complexity: O(n)
bool IsPermutationPalindrome(const std::string &input)
{
  std::unordered_map<char, int> charCount;
  int numNonAlpha = 0;

  for (const char &c : input) {
    // Ignore all non-alpha numeric characters
    if (std::isalpha(c) == false) {
      numNonAlpha++;
      continue;
    }

    if (charCount.find(c) == charCount.end()) {
      // initialize char count to 1
      charCount.insert(std::make_pair(c, 1));
    } else {
      charCount[c]++;
    }
  }

  // Length ignoring non-alpha numeric characters
  bool isLengthOdd = ((input.length() - numNonAlpha)% 2 != 0);
  bool oddCharCountFound = false;

  for (auto it = charCount.begin(); it != charCount.end(); ++it) {
    if (it->second % 2 != 0) {
      if (oddCharCountFound) {
        // More than 1 character is found with odd character count
        return false;
      }

      if (isLengthOdd == false) {
        // Length of the string is even, there can't be any character with
        // odd character count
        return false;
      }

      oddCharCountFound = true;
    }
  }

  return true;
}

int main()
{
  std::string input;
  std::cout << "Input the string: ";
  std::getline(std::cin, input);
  std::cout << std::endl;

  bool result = IsPermutationPalindrome(input);

  std::cout << input << (result ? " is " : " is not ") << "a permutation of a palindrome" << std::endl;

  return 0;
}
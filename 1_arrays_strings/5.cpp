/* One Away
* There are three types of edits that can be performed on strings:
* insert a character, remove a character or replace a character.
* Given two strings, write a function to check if they are one edit
* (or zero edits) away.
* Example:
* pale, ple   -> true
* pales, pale -> true
* pale, bale  -> true
* pale, bae   -> false
*
* Command: g++ -std=c++11 -o 5 5.cpp
*/

#include <iostream>
#include <string>
#include <unordered_map>

void GetCharCounts(const std::string &string, std::unordered_map<char, int> &charCount)
{
  for (const char &c : string) {
    if (charCount.find(c) == charCount.end()) {
      charCount.insert(std::make_pair(c, 1));
    } else {
      charCount[c]++;
    }
  }
}

// If lengths of both strings are same, it is a replacement.
// If lengths of both strings are different, then it is either insertion or deletion.
//
// Form a hash tables with character counts for the both strings.
// As you iterate through the hash table of the first string, subtract the count in both hash tables
// if the char is found in the second hash table.
// Iterate through both the hash tables to see that the char count should one for exactly 1 character.
// Time complexity: O(n)
// Space complexity: O(n)
bool IsOneAway(const std::string &string1, const std::string &string2)
{
  std::unordered_map<char, int> charCount1;
  std::unordered_map<char, int> charCount2;

  GetCharCounts(string1, charCount1);
  GetCharCounts(string2, charCount2);

  for (auto it = charCount1.begin(); it != charCount1.end(); ++it) {
    if (charCount2.find(it->first) != charCount2.end()) {
      charCount2[it->first] -= it->second;
      it->second = 0;
    }
  }

  // When the strings are of equal length, the operation is replacement,
  // in which case, both the character arrays will have 1 character left with
  // count 1. In all other cases, ONLY one of the character arrays will have 1
  // character left with count 1.
  int permittedOneAway = 0;
  if (string1.length() == string2.length()) {
    permittedOneAway = 2;
  } else {
    permittedOneAway = 1;
  }

  for (auto it = charCount1.begin(); it != charCount1.end(); ++it) {
    if (it->second != 0) {
      if (it->second != 1 || permittedOneAway == 0) {
        return false;
      }

      permittedOneAway--;
    }
  }

  for (auto it = charCount2.begin(); it != charCount2.end(); ++it) {
    if (it->second != 0) {
      if (it->second != 1 || permittedOneAway == 0) {
        return false;
      }

      permittedOneAway--;
    }
  }

  return true;
}

int main()
{
  std::string string1;
  std::string string2;

  std::cout << "Enter string 1: ";
  std::getline(std::cin, string1);

  std::cout << "Enter string 2: ";
  std::getline(std::cin, string2);

  bool result = IsOneAway(string1, string2);

  std::cout << string1 << " and " << string2 << (result ? " are " : " are not ") << "one away" << std::endl;
  return 0;
}
/* String Rotation
* Assume you have a method isSubString which checks if one word is a substring of another.
* Given two string, s1 and s2, write code to check if s2 is a rotation of s1 using only one
* call to isSubstring (e.g., "waterbottle" is a rotation of "erbottlewat").
*
* Command: g++ -std=c++11 -o 9 9.cpp
*/

#include <iostream>
#include <string>
#include <cstring>

// Space complexity: O(1)
// Time complexity: O(n)
bool IsSubstring(const std::string &string1, const std::string &string2) 
{
  int length2 = string2.length();
  int pos = 0;
  while (pos < string1.size() - length2) {
    if (string2 == string1.substr(pos, length2)) {
      return true;
    }

    pos++;
  }

  return false;
}

// If string1 = xy, string2 = yx, yx is always a substring of xyxy (i.e., s1s1)
// Space complexity: O(n)
// Time complexity: Depends on IsSubstring
bool IsRotation(const std::string &string1, const std::string &string2)
{
  std::string master = string1 + string1;
  return IsSubstring(master, string2);
}

int main()
{
  std::string string1;
  std::string string2;

  std::cout << "Input string1: ";
  std::getline(std::cin, string1);

  std::cout << "Input string2: ";
  std::getline(std::cin, string2);

  bool result = IsRotation(string1, string2);

  std::cout << string2 << (result ? " is " : " is not ") << "a rotation of " << string1 << std::endl;
  return 0;
}
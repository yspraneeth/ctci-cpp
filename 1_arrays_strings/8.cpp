/* Zero Matrix
* Write an algorithm such that if an element in an MxN matrix is 0, its entire row
* column are set to 0
*
* Command: g++ -std=c++11 -o 8 8.cpp
*/

#include <iostream>

const int ROWS = 4;
const int COLS = 4;

void NullifyRow(int matrix[][COLS], int row)
{
  for (int col = 0; col < COLS; col++) {
    matrix[row][col] = 0;
  }
}

void NullifyCol(int matrix[][COLS], int col)
{
  for (int row = 0; row < ROWS; row++) {
    matrix[row][col] = 0;
  }
}

void PrintMatrix(int matrix[][COLS])
{
  for (int row = 0; row < ROWS; row++) {
    for (int col = 0; col < COLS; col++) {
      std::cout << matrix[row][col];
      if (col == COLS-1) {
        std::cout << std::endl;
      } else {
        std::cout << ", ";
      }
    }
  }
}

// Time complexity: O(n)
// Space complexity: O(1)
int main()
{
  int matrix[ROWS][COLS] = {
    1,2,3,8,
    4,5,0,9,
    2,4,5,7,
    8,0,3,5
  };

  std::cout << "Original matrix:" << std::endl;
  PrintMatrix(matrix);

  // Set the first row/col to 0 if an element is 0
  for (int row = 0; row < ROWS; row++) {
    for (int col = 0; col < COLS; col++) {
      if (matrix[row][col] == 0) {
        matrix[row][0] = 0;
        matrix[0][col] = 0;
      }
    }
  }

  // Nullify all elements of row if the first element is a 0
  for (int row = 0; row < ROWS; row++) {
    if (matrix[row][0] == 0) {
      NullifyRow(matrix, row);
    }
  }

  // Nullify all elements of col if the first element is a 0
  for (int col = 0; col < COLS; col++) {
    if (matrix[0][col] == 0) {
      NullifyCol(matrix, col);
    }
  }

  std::cout << "Modified matrix:" << std::endl;
  PrintMatrix(matrix);

  return 0;
}
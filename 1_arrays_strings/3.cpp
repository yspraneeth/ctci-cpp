/* URLify:
* Write a method of replace all spaces in a string with '%20'. 
* You may assume that the string has sufficient space at the end
* to hold the additional characters, and that you are given the 
* true length of the string.
*
* Command: g++ -std=c++11 -o 3 3.cpp
*/

#include <iostream>
#include <string>
#include <sstream>
#include <cstring>

void Urlify(const std::string &input, const int trueLength, std::string &output)
{
  int numSpaces = 0;
  int charCount = 0;
  std::stringstream ss;

  for (const char &s : input) {
    if (s == ' ') {
      ss << "%20";
    } else {
      ss << s;
    }

    if (charCount++ == trueLength - 1) {
      break;
    }
  }

  output = ss.str();
}

int main()
{
  std::string input;
  int trueLength;
  std::cout << "Enter the string: ";
  std::getline(std::cin, input);

  std::cout << "Enter the trueLength: ";
  std::cin >> trueLength;
  std::cout << std::endl;

  std::string output;
  Urlify(input, trueLength, output);

  std::cout << "URLified string of \"" << input << "\" is \"" << output << "\"" << std::endl;
  return 0;
}
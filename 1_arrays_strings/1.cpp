/*
* Implement a function to determine if a string has all unique
* characters.
* Other solutions (if additional data structure is not to be used):
* 1. Pick a character and compare with all other characters - O(n^2)
* 2. If you are allowed to modify the string in place, sort the string
*    and compare characters - O(n logn)
*
* Command: g++ -std=c++11 -o 1 1.cpp
*/

#include <iostream>
#include <string>
#include <unordered_map>

// Build a hash table of all characters of the string.
// If you find that there exists an entry for a character in the
// hash table, then the string doesn't have unique characters
// Time complexity: O(n)
// Space complexity: O(n)
bool UniqueChars(const std::string &string)
{
  std::unordered_map<char, int> charCount;
  bool result = true;

  for (auto s : string) {
    if (charCount.find(s) == charCount.end()) {
      charCount.insert(std::make_pair(s, 1));
    } else {
      result = false;
      break;
    }
  }

  return result;
}

int main()
{
  std::string input;

  std::cout << "Enter the string: ";
  std::cin >> input;
  std::cout << std::endl;

  bool result = UniqueChars(input);
  std::cout << input << (result ? " has " : " has no ") << "unique characters" << std::endl;

  return 0;
}
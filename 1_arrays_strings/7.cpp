/* Rotate Matrix
* Given an image represented by an NxN matrix, where each pixel in the image is 4bytes, 
* write a method to rotate the image by 90 degrees. Can you do this in place?
*
* Command: g++ -std=c++11 -o 7 7.cpp
*/
#include <iostream>
#include <assert.h>

const int ROWS = 4;
const int COLS = 4;

void PrintMatrix(int matrix[][COLS])
{
  for (int row = 0; row < ROWS; row++) {
    for (int col = 0; col < COLS; col++) {
      std::cout << matrix[row][col];
      if (col == COLS-1) {
        std::cout << std::endl;
      } else {
        std::cout << ", ";
      }
    }
  }
}

// Time Complexity: O(n)
// Space Complexity: O(1)
int main()
{
  assert(ROWS == COLS);

  int matrix[ROWS][COLS] = {
    1,2,3,8,
    4,5,0,9,
    2,4,5,7,
    8,0,3,5
  };

  std::cout << "Original matrix:" << std::endl;
  PrintMatrix(matrix);

  for (int layer = 0; layer < ROWS/2; layer++) {
    int first = layer;
    int last  = ROWS - 1 - layer;

    for (int i = first; i < last; i++) {
      int offset = i - first;

      int top = matrix[first][i];

      // left -> top
      matrix[first][i] = matrix[last - offset][first];

      // bottom->left
      matrix[last - offset][first] = matrix[last][last - offset];

      // right->bottom
      matrix[last][last - offset] = matrix[i][last];

      // top->right
      matrix[i][last] = top;
    }
  }

  std::cout << "Modified matrix:" << std::endl;
  PrintMatrix(matrix);

  return 0;
}
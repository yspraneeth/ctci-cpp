/* 
* Given two strings, write a method to decide if 
* one is a permutation of the other
* Other solutions:
* Sort both the strings and see that they are identical.
* Space complexity: O(1), Time complexity: O(n logn)
*
* Command: g++ -std=c++11 -o 2 2.cpp
*/

#include <iostream>
#include <string>
#include <unordered_map>

// Build a hash table with character counts for string 1.
// For each character in string 2, decrement the character count.
// Finally, iterate through the hash table to make sure all the character
// counts are zeros.
// Time complexity: O(n)
// Space complexity: O(n)
bool IsPermutation(const std::string &string1, const std::string &string2)
{
  std::unordered_map<char, int> charCount;

  for (auto s : string1) {
    if (charCount.find(s) == charCount.end()) {
      charCount.insert(std::make_pair(s, 1));
    } else {
      charCount[s]++;
    }
  }

  for (auto s : string2) {
    if (charCount.find(s) == charCount.end()) {
      // If a character is not found in the map, then it can't be
      // a permutation of string 1
      return false;
    } else {
      charCount[s]--;
    }
  }

  for (auto it = charCount.begin(); it != charCount.end(); ++it) {
    if (it->second > 0) {
      return false;
    }
  }

  return true;
}

int main()
{
  std::string string1;
  std::string string2;

  std::cout << "Enter the first string: ";
  std::cin >> string1;
  std::cout << "Enter the second string: ";
  std::cin >> string2;
  std::cout << std::endl;

  bool result = IsPermutation(string1, string2);

  std::cout << string1 << (result ? " is " : " is not ") \
  << "a permutation of " << string2 << std::endl;
  return 0;
}
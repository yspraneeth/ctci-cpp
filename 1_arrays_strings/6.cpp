/* String compression
* Implement a method to perform basic string compression using the counts of repeated characters.
* For example, string aabcccccaaa would become a2b1c5a3. If the "compressed" string would not
* become smaller than the original string, your method should return the original string.
* You can assume the string has only uppercase and lowercase letters (a-z).
*
* Command: g++ -std=c++11 -o 6 6.cpp
*/

#include <iostream>
#include <string>
#include <sstream>

// Iterate through the input string and maintain char counts for all characters
// Space complexity: O(n)
// Time complexity: O(n)
void StringCompression(const std::string &input, std::string &output)
{
  std::stringstream ss;
  int charCount = 0;

  char prev = '\0';
  for (std::string::size_type i = 0; i < input.size(); i++) {
    if (input[i] != prev) {
      if (i != 0) {
        ss << charCount;
      }

      ss << input[i];
      charCount = 1; // Reset char count for every new character
    } else {
      charCount++;
    }

    prev = input[i];

    // Flush char count for the last character
    if ( i == input.size() -1) {
      ss << charCount;
    }
  }

  output = ss.str();

  if (output.length() == input.length()) {
    output = input;
  }
}

int main()
{
  std::string input;
  std::cout << "Input the string: ";
  std::getline(std::cin, input);
  std::cout << std::endl;

  std::string output;
  StringCompression(input, output);

  if (input.length() == output.length()) {
    std::cout << input << " has not been compressed" << std::endl;
  } else {
    std::cout << input << " has been compressed to " << output << std::endl;
  }

  return 0;
}
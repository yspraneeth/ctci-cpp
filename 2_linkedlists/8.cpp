/* Loop detection
* Given a circular linked list, implement an algorithm that returns the node
* at the beginning of the loop.
* Definition: Circular linked list:
* A (corrupt) linked list in which a node's next pointer points to an earlier node,
* so as to make a loop in the linked list.
* Example: A -> B -> C -> D -> E -> C (same C as earlier)
* Output: C
*/

#include "linkedlist.hpp"

int LoopDetection(LinkedList *list)
{
  Node *fast = list->Head();
  Node *slow = list->Head();

  while (fast != slow) {
    if (fast == nullptr || fast->next == nullptr) {
      return -1;
    }
    fast = fast->next->next;
    slow = slow->next;
    std::cout << "fast " << fast->data << std::endl;
    std::cout << "slow " << slow->data << std::endl;
  } 

  while (fast != nullptr && fast->next != nullptr) {
    fast = fast->next->next;
    slow = slow->next;

    if (fast == slow) {
      break;
    }
  }

  if (fast == nullptr || fast->next == nullptr) {
    return -1;
  }

  slow = list->Head();
  while (fast != slow) {
    fast = fast->next;
    slow = slow->next;
  }

  assert(fast->data == slow->data);
  return fast->data;
}

int main()
{
  LinkedList *list = new LinkedList();

  Node *digit0 = new Node(7);
  Node *digit1 = new Node(1);
  Node *digit2 = new Node(7);
  Node *digit3 = new Node(0);
  list->Append(digit0);
  list->Append(digit1);
  list->Append(digit2);
  list->Append(digit3);

  Node *node = list->Head();
  while(node->next != nullptr) {
    node = node->next;
  }
  node->next = digit1;

  int result = LoopDetection(list);
  std::cout << "The original list" << (result != -1 ? " contains " : " doesn't contain ") << "a loop" << std::endl;
  if (result != -1) {
    std::cout << "Start of the loop is: " << result << std::endl;
  }
  return 0;
}
/* Sum lists
* You have two numbers represented by a linked list, where each node contains
* a single digit. The digits are stored in reverse order, such that the 1's digit
* is at the head of the list. Write a function that adds the two numbers and
* returns the sum as a linked list
* Example:
* (7 -> 1 -> 6) + (5 -> 9 -> 2) = (2 -> 1 -> 9)
* Command: g++ -std=c++11 -o 5 5.cpp
*/
#include "linkedlist.hpp"

void SumReverseOrder(
  LinkedList *operand1, 
  LinkedList *operand2, 
  LinkedList *result)
{
  Node *node1 = operand1->Head();
  Node *node2 = operand2->Head();

  int sum = 0;
  int carry = 0;
  while (node1 != nullptr && node2 != nullptr) {
    sum = node1->data + node2->data + carry;
    carry = (sum > 9);
    Node *res = new Node(sum % 10);
    result->Append(res);

    node1 = node1->next;
    node2 = node2->next;
  }

  while (node1 != nullptr) {
    Node *res = new Node(node1->data);
    result->Append(res);

    node1 = node1->next;
  }

  while (node2 != nullptr) {
    Node *res = new Node(node2->data);
    result->Append(res);

    node2 = node2->next;
  }

  std::cout << "Sum of ";
  operand1->Print();
  std::cout << "and    ";
  operand2->Print();
  std::cout << "is     ";
  result->Print();
}

int SumForwardOrderRecurse(
  Node *node1, 
  Node *node2, 
  LinkedList *result)
{
  if (node1 == nullptr && node2 == nullptr) {
    return 0;
  }

  int carry = SumForwardOrderRecurse(node1->next, node2->next, result);  
  int sum = node1->data + node2->data + carry;
  
  Node *res = new Node(sum % 10);
  result->Prepend(res);

  carry = (sum > 9);
  return carry;
}

void SumForwardOrder(
  LinkedList *operand1, 
  LinkedList *operand2, 
  LinkedList *result)
{
  int length1 = operand1->Length();
  int length2 = operand2->Length();
  int diff = length2 - length1;
  int diffAbs = (diff < 0) ? -diff : diff;

  // Equate the length of both linked lists by prepending with zeros
  if (diff > 0) {
    while (diffAbs > 0) {
      Node *zero = new Node(0);
      operand1->Prepend(zero);
      diffAbs--;
    }
  } else if (diffAbs < 0) {
    while (diffAbs > 0) {
      Node *zero = new Node(0);
      operand2->Prepend(zero);
      diffAbs--;
    }
  }

  int carry = SumForwardOrderRecurse(operand1->Head(), operand2->Head(), result);
  if (carry != 0) {
    Node *msb = new Node(carry);
    result->Prepend(msb);
  }

  std::cout << "Sum of ";
  operand1->Print();
  std::cout << "and    ";
  operand2->Print();
  std::cout << "is     ";
  result->Print();
}

int main()
{
  LinkedList *operand1 = new LinkedList();
  Node *digit0 = new Node(7);
  Node *digit1 = new Node(1);
  Node *digit2 = new Node(6);
  Node *digit6 = new Node(8);
  operand1->Append(digit0);
  operand1->Append(digit1);
  operand1->Append(digit2);
  
  LinkedList *operand2 = new LinkedList();
  Node *digit3 = new Node(5);
  Node *digit4 = new Node(9);
  Node *digit5 = new Node(2);
  operand2->Append(digit3);
  operand2->Append(digit4);
  operand2->Append(digit5);
  operand2->Append(digit6);

  LinkedList *result1 = new LinkedList();
  LinkedList *result2 = new LinkedList();
  std::cout << "Sum in reverse order:" << std::endl;
  SumReverseOrder(operand1, operand2, result1);

  std::cout << std::endl;
  std::cout << "Sum in forward order:" << std::endl;
  SumForwardOrder(operand1, operand2, result2);

  return 0;

}
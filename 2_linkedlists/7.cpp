/* Intersection
* Given two (singly) linked lists, determine if the two lists intersect. Return
* the intersecting node. Note that the intersection is defined based on the 
* reference, not value. i.e., if the kth node of the first linked list is the
* exact same node (by reference) as the jth node of the second linked list, then
* they are intersecting.
*
* Command: g++ -std=c++11 -o 7 7.cpp
*/

#include "linkedlist.hpp"

// Time complexity: O(n)
// Space complexity: O(1)
void Intersection(LinkedList *list1, LinkedList *list2)
{
  Node *tail1 = list1->Head();
  while (tail1->next != nullptr) {
    tail1 = tail1->next;
  }

  Node *tail2 = list2->Head();
  while (tail2->next != nullptr) {
    tail2 = tail2->next;
  }

  if (tail1 != tail2) {
    std::cout << "Lists do not intersect!" << std::endl;
    return;
  }

  int length1 = list1->Length();
  int length2 = list2->Length();
  int diff = length1 - length2;

  Node *head1 = list1->Head();
  Node *head2 = list2->Head();
  if (diff >= 0 ) {
    while (diff != 0) {
      assert(head1 != nullptr);
      head1 = head1->next;
      diff--;
    }
  } else {
    while (diff != 0) {
      assert(head2 != nullptr);
      head2 = head2->next;
      diff++;
    }
  }

  assert(head1 != nullptr && head2 != nullptr);
  while (head1 != head2) {
    head1 = head1->next;
    head2 = head2->next;
  }

  assert (head1 == head2);
  std::cout << "Lists intersect at " << head1->data << std::endl;

}

int main()
{
  LinkedList *list1 = new LinkedList();
  LinkedList *list2 = new LinkedList();

  // Initialize lists
  Node *digit0 = new Node(0);
  Node *digit1 = new Node(1);
  Node *digit2 = new Node(2);
  Node *digit3 = new Node(3);
  Node *digit4 = new Node(4);
  Node *digit5 = new Node(5);
  Node *digit6 = new Node(6);
  Node *digit7 = new Node(7);

  list1->Append(digit0);
  list1->Append(digit1);
  list1->Append(digit3);
  list1->Append(digit2);
  
  list2->Append(digit4);
  list2->Append(digit5);
  list2->Append(digit6);
  list2->Append(digit3);
  list2->Append(digit7);

  std::cout << "List1: ";
  list1->Print();
  std::cout << "List2: ";
  list2->Print();

  Intersection(list1, list2);

  return 0;
}
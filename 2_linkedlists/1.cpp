/* Remove duplicates
* Write code to remove duplicates from an unsorted linked list
* 
* Command: g++ -std=c++11 -o 1 1.cpp
*/

#include "linkedlist.hpp"
#include <unordered_map>

// Time complexity: O(n)
// Space complexity: O(n)
// Other solution: If additional temp data structure such as hash table is not
// allowed, then take two pointers. One of them will be traversing the linked list
// normally while the other serves as a runner which compares all other other nodes
// with the current node. This solution has time complexity of O(n ^2) and space
// complexity of O(1).
void RemoveDuplicates(LinkedList *list)
{
  std::unordered_map<int, bool> hash;
  Node *node = list->Head();
  hash.insert(std::make_pair(node->data, true));
  while (node != nullptr && node->next != nullptr) {
    if (hash.find(node->next->data) == hash.end()) {
      hash.insert(std::make_pair(node->next->data, true));
    } else {
      Node *next = node->next;
      node->next = next->next;
      delete next;
    }

    node = node->next;
  }

  std::cout << "After removing duplicates:" << std::endl;
  list->Print();
}

int main()
{
  LinkedList *list = new LinkedList(0);
  list->Init(10);

  RemoveDuplicates(list);

  return 0;
}
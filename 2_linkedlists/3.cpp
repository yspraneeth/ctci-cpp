/* Delete Middle Node
* Implement an algorithm to delete a middle node (i.e., 
* any node but the first and last node, not necessarily exactly in the middle)
* of a singly linked list, given only access to that node.
* Example: 
* Node c from an input linked list a->b->c->d->e->f
* New linked list a->b->d->e->f
*
* Command: g++ -std=c++11 -o 3 3.cpp
*/

#include "linkedlist.hpp"

int main()
{
  std::cout << "Solution is implemented as LinkedList::RemoveNext()" << std::endl;

  return 0;
}
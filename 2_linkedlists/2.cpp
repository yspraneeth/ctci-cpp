/* Return kth to last element
* Implement an algorithm to find the kth to last element
* of a singly linked list
*
* Command: Command: g++ -std=c++11 -o 2 2.cpp
*/

#include "linkedlist.hpp"
#include <assert.h>

// Advance fast pointer by K positions and then move fast and slow pointers
// 1 step at a time. By the time fast pointer reaches the end, slow pointer
// would be pointing at the kth element from the last
// Time complexity: O(n)
// Space complexity: O(1)
int ReturnKthElement(LinkedList *list, int K)
{
  Node *fast = list->Head();
  Node *slow = list->Head();

  int k = 0;
  while (k < K) {
    fast = fast->next;
    k++;
  }

  while (fast != nullptr) {
    fast = fast->next;
    slow = slow->next;
  }

  return slow->data;
}

int main()
{
  LinkedList *list = new LinkedList(0);
  list->Init(10);

  int length = list->Length();
  std::cout << "Length of the list: " << length << std::endl;
  
  int K;
  std::cout << "Enter which element do you want from the end(K): " << std::endl;
  std::cin >> K;
  assert (K > 0 && K <= length);

  std::cout << K << "th element from the end is: " << ReturnKthElement(list, K) << std::endl;
  return 0;
}
/* Partition
* Write code to partition a linked list around a value x, such that all nodes less than
* x come before all nodes greater than or equal to x. If x is contained within the list, 
* the values of x only need to be after the elements less than x. The parition element
* x can appear anywhere in the "right partition"; it doesn't need to appear between the
* left and right partitions.
* Example:
* Input: 3 -> 5 -> 8 -> 5 -> 10 -> 2 -> 1 [partition = 5]
* Output: 3 -> 1 -> 2 -> 10 -> 5 -> 5 -> 8
*
* Command: g++ -std=c++11 -o 4 4.cpp
*/

#include "linkedlist.hpp"

void Partition(LinkedList *list, int partition)
{
  LinkedList *lesser = new LinkedList();
  LinkedList *greater = new LinkedList();

  Node *node = list->Head();
  while (node != nullptr) {
    Node *newNode = new Node(node->data);

    if (node->data < partition) {
      if (lesser->Head() == nullptr) {
        lesser->Head(newNode);
      } else {
        lesser->Append(newNode);
      }
    } else if (node->data == partition) {
      if (greater->Head() == nullptr) {
        greater->Head(newNode);
      } else {
        greater->Prepend(newNode);
      }
    } else {
      if (greater->Head() == nullptr) {
        greater->Head(newNode);
      } else {
        greater->Append(newNode);
      }
    }
    node = node->next;
  }

  Node *lnode = lesser->Head();
  while (lnode != nullptr && lnode->next != nullptr) {
    lnode = lnode->next;
  }

  // Join both the linked lists
  lnode->next = greater->Head();

  std::cout << "List after partition:" << std::endl;
  lesser->Print();
}

int main()
{
  LinkedList *list = new LinkedList(0);
  list->Init(10);

  int partition;
  std::cout << "Enter the partition element: " << std::endl;
  std::cin >> partition;

  Partition(list, partition);

  return 0;
}
/* Palindrome 
* Implement a function to check if a linked list is a palindrome
*
* Command: g++ -std=c++11 -o 6 6.cpp
*/

#include "linkedlist.hpp"
#include "assert.h"

// Method 1: Iterate through the list with two pointers to identify middle
// of the list. Create a new list with the second half (or first half) of the list. 
// Compare the two lists element by element
//
// Method 2: Iterate through the list with two pointers to identify middle
// of the list. Push the second half of elements into a stack. Starting from the 
// beginning of the list, Compare front of the stack with the list.
//
// Method 3: If the list is a palindrome, then there will be exactly one value of the
// element odd frequency. (this will be the middle of the list). All other values
// should have even frequency
// In all the above 3 approaches, space complexity = O(n)
// time complexity = O(n)
bool IsPalindrome(LinkedList *list)
{
  bool result = true;
  LinkedList *firstHalfReverse = new LinkedList();

  Node *fast = list->Head();
  Node *slow = list->Head();

  while (fast != nullptr && fast->next != nullptr) {
    Node *newNode = new Node(slow->data);
    firstHalfReverse->Prepend(newNode);

    fast = fast->next->next;
    slow = slow->next;
  }

  assert(firstHalfReverse->Length() == list->Length() / 2);

  if (list->Length() % 2 != 0) {
    slow = slow->next;
  }

  Node *first = firstHalfReverse->Head();
  Node *second = slow;

  while (first != nullptr) {
    assert(second != nullptr);
    if (first->data != second->data) {
      result = false;
      break;
    }

    first = first->next;
    second = second->next;
  }

  return result;
}

int main()
{
  LinkedList *list = new LinkedList(0);
  // list->Init(10);
  Node *digit0 = new Node(7);
  // Node *digit1 = new Node(1);
  Node *digit2 = new Node(7);
  Node *digit3 = new Node(0);
  list->Append(digit0);
  // list->Append(digit1);
  list->Append(digit2);
  list->Append(digit3);
  std::cout << "Original list:" << std::endl;
  list->Print();

  bool result = IsPalindrome(list);
  std::cout << "The given list" << (result ? " is " : " is not ") << "a palindrome" << std::endl;
  return 0;
}
/* Linked list
*
*/

#ifndef LINKEDLIST_HPP
#define LINKEDLIST_HPP

#include <iostream>
#include <time.h>

struct Node {
  Node *next;
  int   data;

  Node() {
    next = nullptr;
    data = -1;
  }

  Node(int _data) {
    next = nullptr;
    data = _data;
  }
};

class LinkedList
{
public:
  LinkedList() {
    head = nullptr;
  }

  LinkedList(int data) {
    head = new Node(data);
  }

  ~LinkedList() {
    Node *node = head;

    while (node != nullptr) {
      Node *curr = node;
      Node *next = node->next;
      delete curr;
      node = next;
    }
  }

  void Init(int numElem)
  {
    int i = 0;
    
    srand(time(nullptr));

    Node *node = Head();
    while (++i < numElem) {
      int randInt = rand() % 100;
      Node *newNode = new Node(randInt);

      InsertAfter(node, newNode);
      node = node->next;

      // if (randInt % 2 == 0) {
      //   Node *newNode = new Node(i);
      //   InsertAfter(node, newNode);
      //   node = node->next;
      // }
    }

    std::cout << "Original list:" << std::endl;
    Print();
  }

  void InsertBefore(Node *node, Node *newNode) {
    if (node != nullptr) {
      newNode->next = node;
    } else {
      head = newNode;
    }
  }

  void InsertAfter(Node *node, Node *newNode) {
    if (node != nullptr) {
      node->next = newNode;
    } else {
      head = newNode;
    }
  }

  void Prepend(Node *node) {
    if (head == nullptr) {
      head = node;
    } else {
      node->next = head;
      head = node;
    }
  }

  void Append(Node *node) {
    Node *h = Head();
    if (h == nullptr) {
      head = node;
    } else {
      while (h != nullptr && h->next != nullptr) {
        h = h->next;
      }
      h->next = node;
    }
  }

  void RemoveNext(Node *head) {
    if (head != nullptr && head->next != nullptr) {
      Node *next = head->next;
      head->next = next->next;
      next = nullptr;
    } else {
      std::cerr << "Cannot remove next node because ";
      if (head == nullptr) {
        std::cerr << "given Node is nullptr";
      } else {
        std::cerr << "next node is nullptr";
      }
    }
  }

  void Print() {
    Node *node = head;
    while (node != nullptr) {
      std::cout << node->data << " -> ";
      node = node->next;
    }
    std::cout << "nullptr" << std::endl;
  }

  int Length() {
    if (head == nullptr) {
      return 0;
    }

    Node *node = head;
    int length = 0;
    while (node != nullptr) {
      length++;
      node = node->next;
    }

    return length;
  }

  Node* Head() {
    return head;
  }

  void Head(Node *_head) {
    head = _head;
  }

private:
  Node *head;
};

#endif